import re
import pandas
import os

from pymongo import MongoClient
from bson import ObjectId


class Database:
    DATABASE_URL = 'localhost:27017'

    def __init__(self):
        self.client = MongoClient(self.DATABASE_URL)

    def connect(self):
        return self.client.juridics

    def close(self):
        self.client.close()


class SpreadsheetMaker:
    ROOT_DIR = os.getcwd()
    OUTPUT_DIR = 'output'

    def __init__(self):
        self.database = Database().connect()
        self.metadata = []
        self.root_path = f'{self.ROOT_DIR}/{self.OUTPUT_DIR}/planilha_similaridade.csv'

    def run(self):
        self.__get_acordaos_from_db()
        self.__create_spreadsheet_from_metadata()

    def __get_acordaos_from_db(self):
        acordaos = list(self.database.dataset_similar.find({}))
        counter = 0
        for acordao in acordaos:
            query_striped = re.split(' ', acordao['TEXTO QUERY'])
            if len(query_striped) < 3:
                counter += 1
                pass
            else:
                self.metadata.append({'AREA': acordao['AREA'].strip(),
                                      'TEMA': acordao['TEMA'].strip(),
                                      'TEXTO QUERY': acordao['TEXTO QUERY'].strip(),
                                      'DISCUSSAO': acordao['DISCUSSAO'].strip(),
                                      'SIMILAR': acordao['SIMILAR']})
        print(counter)

    def __create_spreadsheet_from_metadata(self):
        print(len(self.metadata))
        pandas.DataFrame(self.metadata).to_csv(self.root_path, sep='|', encoding='utf-8-sig', index=False)


class Segmenter:

    def __init__(self):
        self.database = Database().connect()
        self.acordaos = None

    def run(self):
        self.__get_acordaos_from_db()
        self.__remove_devices_from_ementa()

    def __get_acordaos_from_db(self):
        self.acordaos = list(self.database.metadata.find({}))

    def __remove_devices_from_ementa(self):
        for acordao in self.acordaos:
            oid = acordao['_id']
            ementa = acordao['EMENTA']
            ementa_splited = re.split('1[.]', ementa, maxsplit=1)
            ementa_cleaned = self.__remove_unwanted_words_from_string(ementa_splited[0])
            verbet = self.__clean_ementa(ementa_cleaned)
            self.__update_verbet_to_database(oid, verbet)

    @staticmethod
    def __remove_unwanted_words_from_string(ementa):
        pattern_list = ('E M E N T A', 'E M E N T A:', 'EMENTA:', 'EMENTA', 'Ementa', 'Ementa:', 'ementa', 'ementa:')
        for item in pattern_list:
            ementa = re.sub(item, ' ', ementa)
        return ementa

    @staticmethod
    def __clean_ementa(ementa):
        ementa = re.sub(' +', ' ', ementa)
        ementa = re.sub('\n', '', ementa)
        ementa = ementa.strip()
        try:
            if ementa[0] == ':' or ementa[0] == '-' or ementa[0] == '"':
                ementa = ementa[1:]
            if ementa[-1] == '.':
                ementa = ementa[0:-1]

        except:
            pass
        return ementa.upper()

    def __update_verbet_to_database(self, oid, verbet):
        query = {'VERBETE': verbet}
        self.database.metadata.update_one({'_id': ObjectId(oid)}, {'$set': query})


class DatasetSimilar:

    def __init__(self):
        self.database = Database().connect()
        self.acordaos = None
        self.new_entitys = []

    def run(self):
        self.__get_acordaos_from_db()
        self.__create_dataset()
        self.__update_new_entitys_to_database()

    def __get_acordaos_from_db(self):
        self.acordaos = list(self.database.metadata.find({}))

    def __create_dataset(self):
        for acordao in self.acordaos:
            new_entity = {
                'AREA': acordao['ÁREA'].upper(),
                'TEMA': acordao['TEMA'].upper(),
                'TEXTO QUERY': acordao['VERBETE'].upper(),
                'DISCUSSAO': acordao['DISCUSSÃO'].upper(),
                'SIMILAR': 1
            }
            self.new_entitys.append(new_entity)

    def __update_new_entitys_to_database(self):
        self.database.dataset_similar.insert_many(self.new_entitys)


class DatasetNaoSimilar:

    def __init__(self):
        self.database = Database().connect()
        self.querys = None
        self.new_entitys = []

    def run(self):
        self.__get_aggregated_querys_from_db()
        self.__create_dataset()
        self.__update_new_entitys_to_database()

    def __get_aggregated_querys_from_db(self):
        aggregated_querys = list(self.database.metadata.aggregate([{"$group": {"_id": {"query": "$DISCUSSÃO"}}}]))
        self.querys = [query['_id']['query'] for query in aggregated_querys]

    def __create_dataset(self):
        for query in self.querys:
            acordaos = list(self.database.metadata.find({"DISCUSSÃO": query}))
            self.__create_entity(acordaos, query)

    def __create_entity(self, acordaos, current_query):
        for query in self.querys:
            for acordao in acordaos:
                if query != current_query:
                    new_entity = {
                                'AREA': acordao['ÁREA'].upper(),
                                'TEMA': acordao['TEMA'].upper(),
                                'TEXTO QUERY': query.upper(),
                                'DISCUSSAO': acordao['DISCUSSÃO'].upper(),
                                'SIMILAR': 0
                    }
                    if new_entity not in self.new_entitys:
                        self.new_entitys.append(new_entity)
                    break

    def __update_new_entitys_to_database(self):
        self.database.dataset_nsimilar.insert_many(self.new_entitys)
