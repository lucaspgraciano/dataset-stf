# Agrupar metadados para treinamento BERT

Segmentador de ementas dos metadados do STF e agrupador de verbetes similares e não similares.


## Get Started
Instale as dependencias executando o comando:
```shell
pip install -r requirement.txt
```

Após instalar as dependencias, verifique a integridade e URL do banco de dados e execute o comando:
```shell
python training.py
```
